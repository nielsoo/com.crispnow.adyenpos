var exec = require('cordova/exec');

exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'adyenPos', 'coolMethod', [arg0]);
};
exports.echo = function(arg0, success, error) {
    exec(success, error, 'adyenPos', 'echo', [arg0]);
};
